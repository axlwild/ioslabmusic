//
//  Session.swift
//  LabTunes
//
//  Created by Axel Cervantes on 11/9/18.
//  Copyright © 2018 Axel Cervantes. All rights reserved.
//

import Foundation

class Session: NSObject{
    var token: String?
    //Singleton Implementation
    static let sharedInstance = Session()
    
    override private init(){
        super.init()
    }
    
    func saveSession(){
        token = "1234567890"
    }
}
