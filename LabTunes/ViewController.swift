//
//  ViewController.swift
//  LabTunes
//
//  Created by Axel Cervantes on 11/9/18.
//  Copyright © 2018 Axel Cervantes. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passWordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    @IBAction func loginButtonTouchUpInside(_ sender: UIButton) {
        guard let username = userNameTextField.text else { return }
        guard let password = passWordTextField.text else { return }
        
        if User.login(userName: username, password: password){
            performSegue(withIdentifier: "loginSuccess", sender: self)
        }
        else {
            return
        }
    }
    
    

    
}

