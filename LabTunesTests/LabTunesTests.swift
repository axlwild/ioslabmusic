//
//  LabTunesTests.swift
//  LabTunesTests
//
//  Created by Axel Cervantes on 11/9/18.
//  Copyright © 2018 Axel Cervantes. All rights reserved.
//

import XCTest
@testable import LabTunes

class LabTunesTests: XCTestCase {

    override func setUp() {
        let session = Session.sharedInstance
        session.token = nil
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testCorrectLogin(){
        XCTAssertTrue(User.login(userName: "iOSLab", password: "Prueba"))
        
    }
    
    func testWrongLogin(){
        XCTAssertFalse(User.login(userName: "MOXith0p", password: "Prueba"))
    }
    
    func testSaveSession(){
        let session = Session.sharedInstance
        let _ = User.login(userName: "iOSLab", password: "223")
        XCTAssertNotNil(session.token)
    }
    
    func testNoSession(){
        let session = Session.sharedInstance
        let _ = User.login(userName: "iOSLdededb", password: "223")
        XCTAssertNil(session.token)
    }
    
    func testExpectedToken(){
        let _ = User.login(userName: "iOSLab", password: "223")
        let session = Session.sharedInstance
        XCTAssertEqual(session.token!, "1234567890","Token Should Match")
        //XCTAssertNotEqual(, , )
    }
    
    func testSessionError(){
        XCTAssertThrowsError(try User.fetchSongs())
    }

}
